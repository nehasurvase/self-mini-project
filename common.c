#include <stdio.h>
#include <string.h>
#include<stdlib.h>
#include "library.h"

void user_accept(user_t *u) {
	//printf("id: ");
	//scanf("%d", &u->id);
	u->id = get_next_user_id();
	printf("Enter Username: ");
	scanf("%s", u->name);
	printf("Enter Email: ");
	scanf("%s", u->email);
	printf("Enter Phone no: ");
	scanf("%s", u->phone);
	printf("Enter password: ");
	scanf("%s", u->password);
	strcpy(u->role, ROLE_MEMBER);
}

void user_display(user_t *u) {
	printf("user id:\t%d \nUser name:\t %s\nEmail:\t %s\nPhone:\t %s \npassword:\t %s\n", u->id, u->name, u->email, u->phone, u->role);
}

// book functions
void book_accept(book_t *b) {
	//printf("id: ");
	//scanf("%d", &b->id);
	printf("Enter Bookname: ");
	scanf("%s", b->name);
	printf("Enter authorname: ");
	scanf("%s", b->author);
	printf("Enter subject: ");
	scanf("%s", b->subject);
	printf("Enter Price: ");
	scanf("%lf", &b->price);
	printf("enter ISBN: ");
	scanf("%s", b->isbn);
}

void book_display(book_t *b) {
	printf("BOOK ID: \t %d\nBOOK NAME:\t %s\nAUTHOR: \t %s\nSUBJECT:\t %s\nPRICE:\t\t %.2lf\nISBN: \t\t %s\n", b->id, b->name, b->author, b->subject, b->price, b->isbn);
}

void bookcopy_accept(bookcopy_t *c) {
	//printf("id: ");
	//scanf("%d", &c->id);
	//printf("bookname: ");
	//scanf("%d", &c->bookname);
	printf("Enter Book id:");
	scanf("%d", &c->bookid);
	//printf("author: ");
	//scanf("%s", c->author);
	printf("Enter Rack no:");
	scanf("%d", &c->rack);
	strcpy(c->status, STATUS_AVAIL);
}

void bookcopy_display(bookcopy_t *c) {
	printf("Copy Id:\t %d\nBook id:\t %d\nRack:\t \t %d\nStatus:\t %s\n", c->id, c->bookid, c->rack, c->status);
}

void issuerecord_accept(issuerecord_t *r) {
	//printf("id: ");
	//scanf("%d", &r->id);
	printf("Enter copy id: ");
	scanf("%d", &r->copyid);
	printf("Enter member id: ");
	scanf("%d", &r->memberid);
	printf("Enter issue date");
	date_accept(&r->issue_date);
	//r->issue_date = date_current();
	r->return_duedate = date_add(r->issue_date, BOOK_RETURN_DAYS);
	memset(&r->return_date, 0, sizeof(date_t));
	r->fine_amount = 0.0;
}

void issuerecord_display(issuerecord_t *r) {
	printf("Issue record: \t\t%d \nCopy id: \t\t%d  \nMember:\t\t %d \nFine: \t\t%.2lf\n", r->id, r->copyid, r->memberid, r->fine_amount);
	printf("issue ");
	date_print(&r->issue_date);
	printf("return due ");
	date_print(&r->return_duedate);
	printf("return ");
	date_print(&r->return_date);
}

void payment_accept(payment_t *p) {
//	printf("id: ");
//	scanf("%d", &p->id);
	printf("Enter Member id: ");
	scanf("%d", &p->memberid);
//	printf("type (fees/fine): ");
//	scanf("%s", p->type);
	strcpy(p->type, PAY_TYPE_FEES);
	printf("amount: ");
	scanf("%lf", &p->amount);
	p->tx_time = date_current();
	//if(strcmp(p->type, PAY_TYPE_FEES) == 0)
		p->next_pay_duedate = date_add(p->tx_time, MEMBERSHIP_MONTH_DAYS);
	//else
	//	memset(&p->next_pay_duedate, 0, sizeof(date_t));
}


void payment_display(payment_t *p) {
	printf("Payment: \t%d \nMember: \t%d \nType:\t\t%s \nAmount: \t%.2lf\n", p->id, p->memberid, p->type, p->amount);
	printf("payment ");
	date_print(&p->tx_time);
	printf("payment due");
	date_print(&p->next_pay_duedate);
}


void password_accept(user_t *u)
{	//give next user id
	u->id = get_next_user_id();
	//printf("enter id whoes password to be change-->");
	//scanf email
	//scanf("%d",u->id);
	//print enter email
	printf("Enter email: ");
	scanf("%s", u->email);
	
	//enter the new password
	printf("enter the new password-->");
	scanf("%s",u->password);
	//sacnf new password
	strcpy(u->role, ROLE_MEMBER);
}

//*******password display******

void password_display(user_t *u)
{
	printf("%d %s",u->id,u->password);
}


void user_add(user_t *u) 
{
	FILE *fp;
	fp = fopen(USER_DB, "ab");
	if(fp == NULL) {
		perror("failed to open users file");
		return;
	}
	fwrite(u, sizeof(user_t), 1, fp);
	printf("user added into file.\n");

	fclose(fp);
}

void book_find_by_name(char name[]) {
	FILE *fp;
	int found = 0;
	book_t b;
	fp = fopen(BOOK_DB, "rb");
	if(fp == NULL) {
		perror("failed to open books file");
		return;
	}
	while(fread(&b, sizeof(book_t), 1, fp) > 0) {
		if(strstr(b.name, name) != NULL) {
			found = 1;
			book_display(&b);
		}
	}
	fclose(fp);
	if(!found)
		printf("No such book found.\n");
}

int user_find_by_email(user_t *u, char email[]) {
	FILE *fp;
	int found = 0;
	fp = fopen(USER_DB, "rb");
	if(fp == NULL) {
		perror("failed to open users file");
		return found;
	}
	while(fread(u, sizeof(user_t), 1, fp) > 0) {
		if(strcmp(u->email, email) == 0) {
			found = 1;
			break;
		}
	}
	fclose(fp);
	return found;
}


int get_next_user_id()
{
	FILE *fp;
	int max = 0;
	int size =sizeof(user_t);
	user_t u;
	fp = fopen(USER_DB, "rb");
	if(fp==NULL)
	return max + 1;
	fseek(fp,-size,SEEK_END);
	if(fread(&u,size,1,fp)>0);
	max = u.id;
	fclose(fp);
	return max + 1;
}

int get_next_book_id()
{
	FILE *fp;
	int max = 0;
	int size =sizeof(book_t);
	book_t u;
	fp = fopen(BOOK_DB, "rb");
	if(fp==NULL)
	return max + 1;
	fseek(fp,-size,SEEK_END);
	if(fread(&u,size,1,fp)>0);
	max = u.id;
	fclose(fp);
	return max + 1;
}


int get_next_bookcopy_id() {
	FILE *fp;
	int max = 0;
	int size = sizeof(bookcopy_t);
	bookcopy_t u;
	fp = fopen(BOOKCOPY_DB, "rb");
	if(fp == NULL)
		return max + 1;
	fseek(fp, -size, SEEK_END);
	if(fread(&u, size, 1, fp) > 0)
		max = u.id;
	fclose(fp);
	return max + 1;
}


int get_next_issuerecord_id() {
	FILE *fp;
	int max = 0;
	int size = sizeof(issuerecord_t);
	issuerecord_t u;
	fp = fopen(ISSUERECORD_DB, "rb");
	if(fp == NULL)
		return max + 1;
	fseek(fp, -size, SEEK_END);
	if(fread(&u, size, 1, fp) > 0)
		max = u.id;
	fclose(fp);
	return max + 1;
}


void edit_profile()
{
	int id, found=0;
	FILE *fp;
	user_t b;
	//input user id from user
	printf("enter user id: ");
    scanf("%d",&id);
	//open user file
	fp=fopen(USER_DB,"rb+");
	if(fp==NULL)
	{
		perror("cannot open users file");
		exit(1);
	}
	// read user one by one and check if user with given id is found.
	while(fread(&b,sizeof(user_t),1,fp)>0)
	{
		if(id==b.id)
		{
			found=1;
			break;
		}
	}
	//if found
	if(found)
	{
		// input new user details from user
		long size=sizeof(user_t);
		user_t nb;
		user_accept(&nb);
		nb.id=b.id;
              strcpy(nb.role, b.role);
		// take file position one record behind.
		fseek(fp,-size,SEEK_CUR);
		// overwrite user details into the file
		fwrite(&nb,size,1,fp);
		printf("profile edited successfully\n");
        printf("New details are: ");
        user_display(&nb);
	}
	else //if not found
	//show message to user that user not found.
	printf("user not found\n");
	//close user file
	fclose(fp);
}


void change_password()
{
	int id, found = 0;
	FILE *fp;
	user_t p;
	//enter details
	printf("\nenter id: \n");
	scanf("%d",&id);
	//open user id file
	fp=fopen(USER_DB,"rb+");
	if(fp == NULL)
	{
		perror("cannot open profile");
		exit(1);
	}
	while (fread(&p, sizeof(user_t),1,fp)>0)
	{
		if(id == p.id)
		{
			found = 1;
			break;
		}
	}
	if(found){
		//input user detail
		long size =sizeof(user_t);
		user_t nu;
		password_accept(&nu);
		nu.id=p.id;
		//position file one record behind
		strcpy(nu.role,p.role);
		fseek(fp,-size,SEEK_CUR);
		//overwrite user details into  the file
		fwrite(&nu, size,1 ,fp);
		printf("password updated.\n");
		password_display(&nu);
	}
	else    //if not found
		printf("profile not found.\n");
		//close file
	fclose(fp);

}


int get_next_payment_id() {
	FILE *fp;
	int max = 0;
	int size = sizeof(payment_t);
	payment_t u;
	// open the file
	fp = fopen(PAYMENT_DB, "rb");
	if(fp == NULL)
		return max + 1;
	// change file pos to the last record
	fseek(fp, -size, SEEK_END);
	// read the record from the file
	if(fread(&u, size, 1, fp) > 0)
		// if read is successful, get max (its) id
		max = u.id;
	// close the file
	fclose(fp);
	// return max + 1
	return max + 1;
}





